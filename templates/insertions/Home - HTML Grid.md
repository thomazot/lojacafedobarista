# Home - HTML Grid

### Inserção

```html 
<div class="home-grids">
	<div class="banner-regua">
        <ul>
            <li class="frete">
                <svg class="icon-frete">
                    <use xlink:href="#icon-cart"></use>
                </svg>
                <span>
                    <strong>frete grátis</strong> para são Paulo
                    <small>*confira as regras</small>
                </span>
            </li>
            <li class="card">
                <svg class="icon-card">
                    <use xlink:href="#icon-card"></use>
                </svg>
                <span>
                    parcele suas compras
                    <strong>em até 6x sem juros</strong>
                </span>
            </li>
            <li class="offer">
                <svg class="icon-boleto">
                    <use xlink:href="#icon-boleto"></use>
                </svg>
                <span>
                    ganhe <strong>5% de desconto</strong>
                    no boleto ou depósito
                </span>
            </li>
        </ul>
    </div>
    <div class="banner-home one">{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="27"}}</div>
    <div class="banner-home two">{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="28"}}</div>
    <div class="banner-home three">{{widget type="cms/widget_block" template="cms/widget/static_block/default.phtml" block_id="29"}}</div>
</div>
```