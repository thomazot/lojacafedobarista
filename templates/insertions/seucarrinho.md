# Sua Sacola 

### DESIGN > Inserções > seucarrinho

#### Link com apenas um item
```html
<a href="{{link_carrinho}}" title="Sua Sacola">
    <svg class="icon"><use xlink:href="#icon-bag"></use></svg>
    <span class="qtd">{{qtd_itens_carrinho}}</span>
    <span class="txt">
        Sua sacola
    	<span>Finalizar</span>
    </span>
</a>
```

#### Link com vários itens
```html
 <a href="{{link_carrinho}}" title="Sua Sacola">
     <svg class="icon"><use xlink:href="#icon-bag"></use></svg>
     <span class="qtd">{{qtd_itens_carrinho}}</span>
     <span class="txt">
         Sua sacola
     	<span>Finalizar</span>
     </span>
 </a>
```

#### Texto do dropdown com o carrinho vazio
```html
 Navegue por nossa loja e <strong>encha sua cesta com as melhores ofertas!</strong>
```

#### Título do dropdown com o carrinho vazio
```html
 <div class="tit empty">Sua Sacola está vazio</div>
```

#### Título do dropdown com o carrinho cheio
```html
 <div class="tit full">Sua Sacola</div>
```

