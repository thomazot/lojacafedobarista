jQuery(document).ready(function($){

    $('.fixed-price .close').click(function(){

        $('.fixed-price').toggleClass('on');

        return false;
    });

    $(window).scroll(function(){
        if($('.product-essential .product-shop').length) {
            if ($(this).scrollTop() > ($('.product-essential .product-shop').offset().top + $('.product-essential .product-shop').height())) {
                $('.fixed-price').addClass('on');
            }
        }

    })

});